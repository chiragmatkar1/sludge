const newman = require('newman'); // require Newman in your project  

// call newman.run to pass `options` object and wait for callback
newman.run({
    collection: require('../fp_collections.json'),
    environment: require('../Fynd X1_environment.json'),
    reporters: [reporter],
}, function (err) {
    if (err) { throw err; }
    console.log('collection run complete!');
});